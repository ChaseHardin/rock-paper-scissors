﻿using System.Web.Mvc;
using RockPaperScissors.Models;

namespace RockPaperScissors.Controllers
{
    public class GameController : Controller
    {
        public ActionResult Play()
        {
            var model = new Game();
            return View(model);
        }

        [HttpPost]
        public ActionResult Play(Game model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            return RedirectToAction("Result", new { userChoice = model.UsersGuess });
        }

        public ActionResult Result(string userChoice)
        {
            var model = new Game { UsersGuess = userChoice };

            model.Play();
            return View(model);
        }
    }
}