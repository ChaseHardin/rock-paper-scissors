﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RockPaperScissors.Models
{
    public class Game
    {
        [Display(Name = "Pick your choice")]
        [Required]
        public string UsersGuess { get; set; }
        public string ComputersGuess { get; set; }
        public string Winner { get; set; }

        public List<string> Options
        {
            get
            {
                return new List<string> { "Rock", "Paper", "Scissors" };
            }
        }

        public void Play()
        {
            var random = new Random().Next(Options.Count);
            ComputersGuess = Options[random];
            Winner = GameLogic(UsersGuess, ComputersGuess);
        }

        public string GameLogic(string user, string computer)
        {
            if (((user == "Rock") && (computer == "Rock")) || ((user == "Paper") && (computer == "Paper")) || ((user == "Scissors") && (computer == "Scissors")))
            {
                return "Tied game";
            }

            if (((user == "Rock") && (computer != "Paper")) || ((user == "Paper") && (computer != "Scissors")) || ((user == "Scissors") && (computer != "Rock")))
            {
                return "You win!";
            }

            return "You Lose!";
        }
    }
}